# archlinux-appstream-data

Arch Linux application database for AppStream-based software centers (Fixed for pamac)

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/rebornos-special-system-files/pamac/archlinux-appstream-data.git
```


This file replaces the original Arch Linux file so that pamac can work properly.

For it to work, the RebornOS repository must be the first in the /etc/pacman.conf file.

